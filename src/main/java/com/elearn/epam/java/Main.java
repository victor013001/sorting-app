package com.elearn.epam.java;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        int argsLength = args.length;

        if (argsLength == 0) {
            System.out.println("Nothing to sort");
            return;
        }

        if (argsLength > 10) {
            throw new IllegalArgumentException("Only 10 numbers can be sort");
        }

        int[] numbers = new int[argsLength];

        try {
            for (int i = 0; i < argsLength; i++) {
                numbers[i] = Integer.parseInt(args[i]);
            }
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Only numbers can be sort");
        }

        Arrays.sort(numbers);

        StringBuilder sortedArray = new StringBuilder();
        sortedArray.append("Sorted numbers: ");
        for (int number : numbers) {
            sortedArray.append(number).append(" ");
        }
        System.out.println(sortedArray);
    }

}