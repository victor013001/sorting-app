import com.elearn.epam.java.Main;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SortingAppLettersArgsTest {

    protected Main sortingApp = new Main();

    private String[] args;

    public SortingAppLettersArgsTest(String[] args) {
        this.args = args;
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {new String[]{"98", "56", "one", "34", "75", "6", "3", "1", "2", "8"}},
                {new String[]{"18", "two", "97", "0", "25", "65", "34", "1", "17", "12"}},
                {new String[]{"57", "ten", "56", "12", "0", "1", "9", "8", "101", "75"}}
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void tenArgsTest() {
        sortingApp.main(args);
    }
}
