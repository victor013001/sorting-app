import com.elearn.epam.java.Main;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SortingAppPlusTenArgsTest {
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    protected Main sortingApp = new Main();

    private String[] args;

    public SortingAppPlusTenArgsTest(String[] args) {
        this.args = args;
    }

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @After
    public void tearDown() {
        System.setOut(standardOut);
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {new String[]{"98", "56", "1", "34", "75", "6", "3", "1", "2", "8", "54"}},
                {new String[]{"18", "43", "97", "0", "25", "65", "34", "1", "17", "12", "86", "94"}},
                {new String[]{"57", "300", "56", "12", "0", "1", "9", "8", "101", "75", "16", "12", "84"}}
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void tenArgsTest() {
        sortingApp.main(args);
    }
}
