import com.elearn.epam.java.Main;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SortingAppZeroArgsTest {

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    protected Main sortingApp = new Main();

    private String[] args;
    private String sortedExpected;

    public SortingAppZeroArgsTest(String[] args, String sortedExpected) {
        this.args = args;
        this.sortedExpected = sortedExpected;
    }

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @After
    public void tearDown() {
        System.setOut(standardOut);
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {new String[]{}, "Nothing to sort"},
                {new String[]{}, "Nothing to sort"},
                {new String[]{}, "Nothing to sort"}
        });
    }

    @Test
    public void tenArgsTest() {
        sortingApp.main(args);
        Assert.assertEquals(sortedExpected, outputStreamCaptor.toString().trim());
    }
}
